from danspeech import Recognizer
from danspeech.pretrained_models import TestModel
from danspeech.audio.resources import Microphone

# Get a list of microphones found by PyAudio
mic_list = Microphone.list_microphone_names()
mic_list_with_numbers = list(zip(range(len(mic_list)), mic_list))
print("Available microphones: {0}".format(mic_list_with_numbers))

# Choose the microphone
mic_number = input("Pick the number of the microphone you would like to use: ")

# Init a microphone object
m = Microphone(sampling_rate=16000, device_index=int(mic_number))

# Init a DanSpeech model and create a Recognizer instance
model = TestModel()
recognizer = Recognizer(model=model)

print("Speek a lot to adjust silence detection from microphone...")
with m as source:
    recognizer.adjust_for_speech(source, duration=5)

# Enable streaming
recognizer.enable_streaming()

# Create the streaming generator which runs a background thread listening to the microphone stream
generator = recognizer.streaming(source=m)

# The below code runs for a long time. The generator returns transcriptions of spoken speech from your microphone.
print("Speak")
for i in range(100000):
    trans = next(generator)
    print(trans)