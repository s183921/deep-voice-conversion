# Deep Voice Conversion and Ethical Considerations of such Technology 
The project will implement and try to advance state-of  -the-art voice transfer systems using deep learning (see alsohttps://paperswithcode.com/task/voice-conversion) to investigate what level of voice transfer quality can be achieved within the limited time-frame of the project. The project will further reflect upon ethical considerations of such systems in regards to both potential use and misuses.

## Group Members
August Semrau
Karl Ulbæk
William Marstrand


## Important Links
* Log: https://docs.google.com/document/d/1sYNDpw3-cLa_bMVms8yS96UQpGujsyUNdrAQ9TO2C8E/edit
* Collaboration Agreement: https://docs.google.com/document/d/1Daad3xrM6UQaDOeNAgyjuxhN2Sr7BvNapXCV-DPDnyM/edit
* Gantt Chart: https://docs.google.com/spreadsheets/d/12AEEpt98fZO_CBTTWIlemPfmvYsdZvkyVbLedJ7y9xY/edit#gid=0
